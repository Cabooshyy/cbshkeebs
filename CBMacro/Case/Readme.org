#+TITLE: CB-Macro Case Files
#+DESCRIPTION: All of the STL's and STEP files for the CB-Macro
#+AUTHOR: Cabooshy (Cameron Miller)
#+STARTUP: showeverything

this folder houses all of the 3D Models for the CB-Macro, premade STL files are here for you to download and print, there is also a STEP file
for the case and switch plate to allow you to remix the macropad, should you wish.
